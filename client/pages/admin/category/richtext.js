import { useState, useEffect } from 'react';
import axios from 'axios';

import dynamic from 'next/dynamic';
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });
// import 'react-quill/dist/quill.bubble.css';
import 'react-quill/dist/quill.snow.css';

import { API } from '../../../config';
import { showSuccessMessage, showErrorMessage } from '../../../helpers/alerts';
import Layout from '../../../components/Layout';
import withAdmin from '../../withAdmin';

const Richtext = ({ user, token }) => {
    // state
    const [name, setName] = useState('');
    const [content, setContent] = useState('');

    const handleName = e => {
        setName(e.target.value);
    };
    const handleContent = e => {
        // console.table(typeof e)
        setContent(e);
    };
   
    const handleSubmit = async e => {
        e.preventDefault();
        // console.table({ name, content});
    };
    
    return (
        <Layout>

            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label className="text-muted">Name</label>
                    <input onChange={handleName} value={name} type="text" className="form-control" required />
                </div>
                <div className="form-group">
                    <label className="text-muted">Content</label>
                    <ReactQuill
                        value={content}
                        onChange={handleContent}
                        placeholder="Write something..."
                        // theme="bubble"
                        theme="snow"
                        className="pb-5 mb-3"
                        style={{ border: '1px solid #666' }}
                    />
                </div>
                <div>
                    <button className="btn btn-default">Submit</button>
                </div>
            </form>

        </Layout>
  );
};

export default withAdmin(Richtext);
