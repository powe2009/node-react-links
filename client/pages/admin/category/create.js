import { useState, useEffect } from 'react';
import axios from 'axios';

import dynamic from 'next/dynamic';
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });
// import 'react-quill/dist/quill.bubble.css';
import 'react-quill/dist/quill.snow.css';

import { API } from '../../../config';
import { showSuccessMessage, showErrorMessage } from '../../../helpers/alerts';
import Layout from '../../../components/Layout';
import withAdmin from '../../withAdmin';

const Create = ({ user, token }) => {
    const [state, setState] = useState({
        name: '',
        content: '',
        error: '',
        success: '',
        formData: process.browser && new FormData(),
        buttonText: 'Create',
        imageUploadText: 'Upload image'
    });

    const { name, image, success, error, formData, buttonText, imageUploadText } = state;
    const [content, setContent] = useState('');

    const handleChange = name => e => {
        const value = name === 'image' ? e.target.files[0] : e.target.value;
        const imageName = name === 'image' ? e.target.files[0].name : 'Upload image';
        
        // The difference between set() and append() is that if the specified key does already exist, 
        // set() will overwrite all existing values with the new one. whereas FormData. append will append 
        // the new value onto the end of the existing set of values.
        //
        formData.set(name, value);
        setState({ ...state, [name]: value, error: '', success: '', imageUploadText: imageName });
    };

    const handleContent = e => {
        setContent(e);
        setState({ ...state, success: '', error: '' });
    };

    const handleSubmit = async e => {
        e.preventDefault();
        setState({ ...state, buttonText: 'Creating' });
        formData.append('content', content);
        // console.table(formData);

        try {
            const response = await axios.post(`${API}/category`, formData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log('CATEGORY CREATE RESPONSE', response);
            setState({
                ...state,
                name: '',
                content: '',
                formData: '',
                buttonText: 'Created',
                imageUploadText: 'Upload image',
                success: `${response.data.name} is created`
            });
        } catch (error) {
            console.log('CATEGORY CREATE ERROR', error);
            setState({ ...state, name: '', buttonText: 'Create', error: error.response.data.error });
        }
    };

    const createCategoryForm = () => (
        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input onChange={handleChange('name')} 
                    value={name} 
                    type="text" className="form-control" 
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Content</label>
                <ReactQuill
                    value={content}
                    onChange={handleContent}
                    placeholder="Write something..."
                    theme="snow"
                    className="pb-5 mb-3"
                    style={{ border: '1px solid #666' }}
                />
            </div>

            <div className="form-group">
                <label className="btn btn-outline-secondary">
                    {imageUploadText}
                    <input
                        onChange={handleChange('image')}
                        type="file"
                        accept="image/*"
                        className="form-control"
                        hidden
                    />
                </label>
            </div>
            <div>
                <button className="btn btn-outline-warning">{buttonText}</button>
            </div>
        </form>
    );

    return (
        <Layout>
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <h1>Create category</h1>
                    <br />
                    {success && showSuccessMessage(success)}
                    {error && showErrorMessage(error)}
                    {createCategoryForm()}
                </div>
            </div>
        </Layout>
    );
};

export default withAdmin(Create);
