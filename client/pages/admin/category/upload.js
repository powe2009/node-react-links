import { useState, useEffect } from 'react';
import axios from 'axios';
import Resizer from 'react-image-file-resizer';

import { API } from '../../../config';
import { showSuccessMessage, showErrorMessage } from '../../../helpers/alerts';
import Layout from '../../../components/Layout';
import withAdmin from '../../withAdmin';

// https://dev.to/asimdahall/client-side-image-upload-in-react-5ffc
//
const Upload = ({ user, token }) => {
    // useRef can persist a value for a full lifetime of the component.  However, note 
    // that the component will not rerender when the current value of useRef changes.
    //
    const uploadedImage = React.useRef(null);

    const handleImageUpload = e => {
        const [file] = e.target.files;
        if (file) {
            const reader = new FileReader();
            const {current} = uploadedImage;
            current.file = file;
            reader.onload = (e) => {
                current.src = e.target.result;
            }
            reader.readAsDataURL(file);
        }
    };
  
    return (
      
      <div>

          <input type="file" 
            accept="image/*" 
            onChange={handleImageUpload} 
            style={{ 
              display: 'inline-block',
              padding: '6px 12px',
              cursor: 'pointer' }}
          />

          <div
              style={{
                height: "100px",
                width: "100px",
                display: 'block',
                marginLeft: 'auto',
                marginRight: 'auto',
                width: '50%'
              }}
            >
         
            <img
              ref={uploadedImage}
              style={{ width: '100px', height: 'auto' }}
            />
        </div>

    </div>
  );
};

export default withAdmin(Upload);
